<?php

namespace Tokk\Uploader\File;

class FileType
{
    const IMAGE = 'IMAGE';
    const FILE = 'FILE';

    private function __construct() {}

    private function __clone() {}

    private function __wakeup() {}
}